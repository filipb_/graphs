class Vertex(object):
    def __init__(self):
        self._neighbours = []

    def add_neighbour(self, v):
        self._neighbours.append(v)

    def neighbours(self, v):
        return v in self._neighbours
