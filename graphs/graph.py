import math
from .edge import Edge
from PIL import Image, ImageDraw


class Graph(object):
    """A graph."""

    def __init__(self):
        self.vertices = []
        self.edges = []

    def add_vertex(self, v):
        self.vertices.append(v)

    def add_edge(self, vi1, vi2):
        v1 = self.vertices[vi1]
        v2 = self.vertices[vi2]
        e = Edge(v1, v2)
        self.edges.append(e)
        v1.add_neighbour(v2)
        v2.add_neighbour(v1)

    def adjacency_matrix(self):
        for v1 in self.vertices:
            for v2 in self.vertices:
                s = '1' if v1.neighbours(v2) else '0'
                print('%s ' % s, end='')
            print('')

    def adjacency_list(self):
        for i, v1 in enumerate(self.vertices):
            print('%s: ' % i, end='')
            l = []
            for j, v2 in enumerate(self.vertices):
                if v1.neighbours(v2):
                    l.append(str(j))
            print('[%s]' % ', '.join(l))

    def incidence_matrix(self):
        for i, e in enumerate(self.edges):
            for j, v in enumerate(self.vertices):
                s = '1' if e.contains(v) else '0'
                print('%s ' % s, end='')
            print('')

    def to_image(self, path, size=800):
        im = Image.new('RGB', (size, size))
        d = 2 * math.pi / len(self.vertices)
        r = 200
        vertex_color = (255, 0, 0)
        edge_color = (0, 0, 255)

        def cal_pos(i):
            x = im.size[0] / 2 + r * math.sin(d * i)
            y = im.size[1] / 2 + r * math.cos(d * i)
            return x, y

        draw = ImageDraw.Draw(im)
        for i, v1 in enumerate(self.vertices):
            x, y = cal_pos(i)
            draw.ellipse([(x - 5, y - 5), (x + 5, y + 5)], vertex_color, vertex_color)

            for j, v2 in enumerate(self.vertices):
                if v1.neighbours(v2):
                    x1, x2 = cal_pos(j)
                    draw.line((x, y, x1, x2), fill=edge_color)
                    
        del draw
        im.save(path, "PNG")
