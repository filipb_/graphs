class Edge(object):
    def __init__(self, v1, v2):
        self.v1 = v1
        self.v2 = v2

    def contains(self, v):
        return self.v1 == v or self.v2 == v

    def __eq__(self, e):
        return (self.v1 == e.v1 and self.v2 == e.v2) or \
               (self.v1 == e.v2 and self.v2 == e.v1)
