from setuptools import setup

setup(
    name='graphs',
    version='0.0.0-dev',
    license='BSD',
    packages=[
        'graphs',
    ],
    install_requires=[
        'pytest',
        'pillow',
    ],
)
