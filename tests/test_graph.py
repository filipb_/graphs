from graphs import Graph, Vertex

def test_graph():
    g = Graph()
    for i in range(5):
        v = Vertex()
        g.add_vertex(v)
    assert len(g.vertices) == 5

def test_edges():
    g = Graph()

    for i in range(5):
        v = Vertex()
        g.add_vertex(v)

    g.add_edge(0, 1)
    g.add_edge(1, 2)

    print()
    g.adjacency_matrix()
    print()
    g.adjacency_list()
    print()
    g.incidence_matrix()

    g.to_image('/tmp/test.png')
